// 1. Create an activity.js file on where to write and save the solution for the activity.
// 2. Create a Database of hotel with a collection of rooms
// 3. Insert a single room (insertOne method) with the following details: 
    // a. name- single
    // b. accommodates- 2
    // c. price - 1000
    // d. description- A simple room with all the basic necessities 
    // e. rooms_available- 10
    // f. isAvailable- false

    // ***SOLUTION***
        // *****insertOne method*****
db.users.insertOne(
    {
        "name": "single",
        "accommodates": 2,
        "price": 1000,
        "description": "A simple room with all the basic necessities" ,
        "rooms_available": 10,
        "isAvailable": false
    }
);

// 4. Insert multiple rooms (insertMany method) with the following details:
    // a. name- double
    // b. accommodates- 3
    // c. price - 2000
    // d. description- A room fit for a small family going on a  vacation 
    // e. rooms_available- 5
    // f. isAvailable- false 

    // a. name- queen
    // b. accommodates- 4
    // c. price - 4000
    // d. description- A room with a queen size bed perfect for a simple gateway
    // e. rooms_available- 15
    // f. isAvailable- false

    // ***SOLUTION***
    db.users.insertMany(
        [
            {
                    "name": "double",
                    "accommodates": 3,
                    "price": 2000,
                    "description": "A room fit for a small family going on a  vacation",
                    "rooms_available": 5,
                    "isAvailable": false
            }, 
            {
                    "name": "queen",
                    "accommodates": 4,
                    "price": 4000,
                    "description": "A room with a queen size bed perfect for a simple gateway",
                    "rooms_available": 15,
                    "isAvailable": false
            }, 
        ]
    );


// 5. Use the find method to search for a room with the name double

        // ***SOLUTION***
        db.users.find({"_id" : ObjectId("61e84c49ffac35967688d696")})

// 6. use the updateOne method to update the queen room and set the available room to 0

        // ***SOLUTION***
        db.users.updateOne(
            {"_id" : ObjectId("61e84c49ffac35967688d697")},
            {
                $set: {
                    "rooms_available": 0
                }
            }
        );
// 7. use the deleteMany method rooms to delete all rooms that have 0 availability.

db.users.deleteMany(
    {"rooms_available": 0} // run in RoboDT to delete the primary entry
)

// 8. Create a git repository named s24
// 9. Initialize a local git repository, add the remote link and push to git with a commit message of 'Add activity code'.
// 10. Add the link in Boodle.


