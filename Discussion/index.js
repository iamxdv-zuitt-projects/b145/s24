// *****insertOne method*****
db.users.insertOne(
    {
        "firstName": "Jane",
        "lastName": "Doe",
        "age": 21,
        "contact": {
            "phone": "87654321",
            "email": "janedoe@gmail.com"
        },
        "courses": ["CSS", "Javascript", "Python"],
        "department": "none"
    }
); //paste to Robo3T and run

//------------------------------------------------------------------------------------------

// *****insertMany method*****

db.users.insertMany(
    [
        {
                "firstName": "Stephen",
                "lastName": "Hawking",
                "age": 76,
                "contact": {
                        "phone": "87654321",
                        "email": "stephenhawking@gmail.com"
                },
                "courses": ["Python", "React", "PHP"],
                "department": "none"
        }, 
        {
                "firstName": "Neil",
                "lastName": "Armstrong",
                "age": 82,
                "contact": {
                        "phone": "87654321",
                        "email": "neilarmstrong@gmail.com"
                },
                "courses": ["React", "Laravel", "Sass"],
                "department": "none"
        }, 
    ]
);  //paste to Robo3T and run
//------------------------------------------------------------------------------------------

// *****Read Operation*****
    // -retrieves documents from the collection

    // db.collections.find({query}, {field})

    db.users.find(); //paste to Robo3T and run
//------------------------------------------------------------------------------------------

// *****Update Operation*****
    // -update a document/s

    // db.collections.updateOne({filter}, {update})
    // db.collections.updateMany()
// ==================================================

// Example: 
    db.users.insertOne(
        {
            "firstName": "Test",
            "lastName": "Test",
            "age": 0,
            "contact": {
                    "phone": "0",
                    "email": "test@gmail.com"
            },
            "courses": [],
            "department": "none"
        }
    ); //paste to Robo3T and run. This will serve as primary entry

    
    // modifying the newly added document using updateOne() method
    //=============================================================
    //Syntax:   
    // db.users.updateOne(
    //     {filter},
    //     {update}
    // );

    //use first name field as a filter and look for the name test
	//using update operator set, update the fields of the matching document with the following details
		// bill gates, 65yo, phone: 12345678, bill@gmail.com, courses PHP, Laravel, HTML, operations dept, status: active

    // Answer

    db.users.updateOne(
		{"firstName": "Test"},
		{
			$set: {
				"firstName": "Bill",
				"lastName": "Gates",
				"age": 65,
				"contact": {
					"phone": "12345678",
					"email": "bill@gmail.com"
				},
				"courses": ["PHP", "Laravel", "HTML"],
				"department": "Operations",
				"status": "active"
			}
		}
	); // To check: run find() and expected output should be the 'Test' will change to above given input.


    // updateMany()
            // use department field as a filter and loo for value none
            // using update operator, update the department filed from value none to hr
            // run find method to check if department fields of the matching document is updated

            // Answers
db.users.updateMany(
    {"department": "none"},
    {
        $set: {
            "department": "HR"
        
        }
    }
); // To check: run find() and expected output should be there will be 'HR' in the department section.

// Mini activity
    // look for a document that has a field status, using object id as filter
    // remote the status field using update operator

    //Answers
    db.users.updateOne(
        {"_id" : ObjectId("61e80604d22e93d04cb88356")},
        {
            $unset: {
                "status": "active"
            }
        }
    ); //Paste and run it to your RoboDT >> then run the find() again and see the changes
        // Expected output will remove the status: active.
    //Note that the ObjectID is unique. Hence copy what appears to your machine or in the MongoDB collection Data.

// ---------------------------------------------------------------------------------------

    // *****Delete Operation*****
        // - delete a document/s

        // db.collections.deleteOne()
        // db.collections.deleteMany()

        // insert a document as an example to be deleted

        db.users.insertOne({"firstName": "John", "lastName": "Devs" }) //paste and run first as example entry
        // To check: run the find() again to see if it appears.

        // For deleting
        db.users.deleteOne(
            {"firstName": "John", "lastName": "Devs"} // run in RoboDT to delete the primary entry
        )